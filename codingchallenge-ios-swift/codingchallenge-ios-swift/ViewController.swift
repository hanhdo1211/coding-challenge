//
//  ViewController.swift
//  codingchallenge-ios-swift
//
//  Created by Hanh Do on 5/16/17.
//  Copyright © 2017 H2MobileDev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var loadingView: LoadingView?
    
    var posts = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setupLoadingView()
        self.setupRefreshControl() 
        self.getPostsData()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.storePostsData), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.storePostsData()
        NotificationCenter.default.removeObserver(self)
        self.hideLoadingView()
    }
    
    func storePostsData() {
        Utility.storePostsLocal(posts: self.posts)
    }
    
    func getPostsData() {
        self.posts = Utility.getPostsLocal()
        self.tableView.reloadData()
        
        self.showLoadingView()
        NetworkManager.sharedInstance.getAllPost {[weak self] (response) in
            self?.hideLoadingView()
            if let postsResponse = response {
                self?.posts = postsResponse
                Utility.storePostsLocal(posts: postsResponse)
                self?.tableView.reloadData()
            } else {
                print("erorr")
            }
        }
    }
    
    func setupRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ViewController.refresh(_:)), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
    }
    
    func setupLoadingView() {
        self.loadingView = Bundle.main.loadNibNamed(LoadingView.className, owner: self, options: nil)?[0] as? LoadingView
        if let view = self.loadingView {
            view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
        }
    }
    
    func showLoadingView() {
        guard let strongLoadingView = self.loadingView else { return }
        
        guard strongLoadingView.superview == nil else { return }
        
        if let view = self.navigationController?.view {
            view.addSubview(strongLoadingView)
        } else {
            self.view.addSubview(strongLoadingView)
        }
        strongLoadingView.indicatorView.startAnimating()
    }
    
    func hideLoadingView() {
        guard let strongLoadingView = self.loadingView else { return }
        
        guard strongLoadingView.superview != nil else { return }
        
        strongLoadingView.indicatorView.stopAnimating()
        strongLoadingView.removeFromSuperview()
    }
    
    func refresh(_ refreshControl: UIRefreshControl) {
        self.getPostsData()
        refreshControl.endRefreshing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: MomentItemTableViewCell.className, for: indexPath) as? MomentItemTableViewCell {
            print("cellForRowAt \(indexPath)")
            
            if let castedCell = cell as? MomentItemTableViewCell {
                
                let postData = self.posts[indexPath.row]
                castedCell.tag = indexPath.row
                castedCell.userFullNameLabel.text = postData.user.fullName
                castedCell.postDateLabel.text = postData.getPostTime()
                castedCell.postMessageLabel.text = postData.message
                
                castedCell.setIconPostLike(withState: postData.isLiked)
                
                castedCell.postImageImageView.image = postData.image
                castedCell.userAvatarImageView.image = postData.user.image
                castedCell.postLikeCountButton.setTitle("\(postData.likeCount)", for: .normal)
                castedCell.postCommentCountButton.setTitle("\(postData.commentCount)", for: .normal)
                
                castedCell.delegate = self
                
                // Download post image
                NetworkManager.sharedInstance.imageFromURL(url: postData.imageURL, onCompletion: {[copyIndexPath = indexPath, weak self] (imageDowloaded) in
                    guard let weakSelf = self else { return }
                    guard castedCell.tag == copyIndexPath.row else { return }
                    weakSelf.posts[copyIndexPath.row].image = imageDowloaded
                    castedCell.postImageImageView.image = imageDowloaded
                })
                
                // Download user avatar image
                NetworkManager.sharedInstance.imageFromURL(url: postData.user.imageURL, onCompletion: {[copyIndexPath = indexPath, weak self] (imageDowloaded) in
                    guard let weakSelf = self else { return }
                    guard castedCell.tag == copyIndexPath.row else { return }
                    weakSelf.posts[copyIndexPath.row].user.image = imageDowloaded
                    castedCell.userAvatarImageView.image = imageDowloaded
                })
                
                // Get Post Like count
                NetworkManager.sharedInstance.getLikeCount(ofPost: postData, onCompletion: {[copyIndexPath = indexPath, weak self] (likeCount) in
                    guard let weakSelf = self else { return }
                    guard castedCell.tag == copyIndexPath.row else { return }
                    weakSelf.posts[copyIndexPath.row].likeCount = likeCount
                    
                    if let itemCell = weakSelf.tableView.cellForRow(at: copyIndexPath) as? MomentItemTableViewCell {
                        itemCell.postLikeCountButton.setTitle("\(likeCount)", for: .normal)
                    }
                })
                
                // Get Post Comment count
                NetworkManager.sharedInstance.getCommentCount(ofPost: postData, onCompletion: {[copyIndexPath = indexPath, weak self] (commentCount) in
                    guard let weakSelf = self else { return }
                    guard castedCell.tag == copyIndexPath.row else { return }
                    weakSelf.posts[copyIndexPath.row].commentCount = commentCount
                    
                    if let itemCell = weakSelf.tableView.cellForRow(at: copyIndexPath) as? MomentItemTableViewCell {
                        itemCell.postCommentCountButton.setTitle("\(commentCount)", for: .normal)
                    }
                })
            }
            return cell
        }
        return UITableViewCell()
    }
    
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("willDisplay \(indexPath)")
        
        
    }
}

extension ViewController: MomentItemTableViewCellDelegate {
    func didSelectedLikePostButton(cell: MomentItemTableViewCell) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            self.posts[indexPath.row].isLiked = !self.posts[indexPath.row].isLiked
            cell.setIconPostLike(withState: self.posts[indexPath.row].isLiked)
        }
    }
    
}
