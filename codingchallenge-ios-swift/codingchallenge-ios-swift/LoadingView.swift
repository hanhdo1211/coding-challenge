//
//  LoadingView.swift
//  codingchallenge-ios-swift
//
//  Created by Hanh Do on 5/18/17.
//  Copyright © 2017 H2MobileDev. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
