//
//  Model.swift
//  codingchallenge-ios-swift
//
//  Created by Hanh Do on 5/17/17.
//  Copyright © 2017 H2MobileDev. All rights reserved.
//

import Foundation
import UIKit

struct Post {
    let id: Int
    var user: User
    let message: String
    let postedAt: Date
    let imageURL: URL
    var image: UIImage
    var likeCount: Int
    var commentCount: Int
    var isLiked: Bool
    
    let dateFormatString = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    
    init?(json: [String: Any]) {
        guard let id = json["_id"] as? Int,
            let message = json["message"] as? String,
            let postedAtString = json["postedAt"] as? String,
            let imageURLString = json["imageURL"] as? String,
            let userJson = json["user"] as? [String: Any]
        else {
            print("init post error")
            return nil
        }
        
        // Try to get Image URL
        guard let imageURL = URL(string: imageURLString) else { return nil }
        // Try to get User data
        guard  let user = User(json: userJson) else { return nil }
        // Try to get Post date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = self.dateFormatString
        guard let postedAtDate = dateFormatter.date(from: postedAtString) else { return nil }
        
        self.message = message
        self.id = id
        self.postedAt = postedAtDate
        self.imageURL = imageURL
        self.user = user
        
        // init value
        if let commentCount = json["commentCount"] as? Int {
            self.commentCount = commentCount
        } else {
            self.commentCount = 0
        }
        
        if let likeCount = json["likeCount"] as? Int {
            self.likeCount = likeCount
        } else {
            self.likeCount = 0
        }
        
        if let isLiked = json["isLiked"] as? Bool {
            self.isLiked = isLiked
        } else {
            self.isLiked = false
        }
        
        self.image = UIImage()
    }
    
    func getPostTime() -> String {
        var result = ""
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        
        let now = Date()
        
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute, .second], from: self.postedAt, to: now)
        
        if let year = components.year, year > 0 {
            formatter.allowedUnits = .year
        } else if let month = components.month, month > 0 {
            formatter.allowedUnits = .month
        } else if let weekOfMonth = components.weekOfMonth, weekOfMonth > 0 {
            formatter.allowedUnits = .weekOfMonth
        } else if let day = components.day, day > 0 {
            formatter.allowedUnits = .day
        } else if let hour = components.hour, hour > 0 {
            formatter.allowedUnits = .hour
        } else if let minute = components.minute, minute > 0 {
            formatter.allowedUnits = .minute
        } else {
            formatter.allowedUnits = .second
        }
        
        let formatString = NSLocalizedString("%@ ago", comment: "Used to say how much time has passed. e.g. '2 hours ago'")
        
        if let timeString = formatter.string(from: components) {
            result = String(format: formatString, timeString)
        }
        
        return result
    }
    
    func toDictionary() -> [String: Any] {
        var result = [String: Any]()
        result["_id"] = self.id
        result["message"] = self.message
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = self.dateFormatString
        result["postedAt"] = dateFormatter.string(from: self.postedAt)
        
        result["imageURL"] = self.imageURL.absoluteString
        result["likeCount"] = NSNumber(integerLiteral: self.likeCount)
        result["commentCount"] = NSNumber(integerLiteral: self.commentCount)
        result["isLiked"] = self.isLiked
        
        result["user"] = self.user.toDictionary()
        
        return result
    }
}

struct User {
    let id: Int
    let fullName: String
    let mobileNo: String
    let dob: Date
    let imageURL: URL
    var image: UIImage
    
    let dateFormatString = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    
    init?(json: [String: Any]) {
        guard let id = json["_id"] as? Int,
            let fullName = json["fullName"] as? String,
            let mobileNo = json["mobileNo"] as? String,
            let dobString = json["dob"] as? String,
            let imageURLString = json["imageURL"] as? String
            else {
                print("init post error")
                return nil
        }
        // Try to get Image URL
        guard let imageURL = URL(string: imageURLString) else { return nil }
        // Try to get DOB date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = self.dateFormatString
        guard let dobDate = dateFormatter.date(from: dobString) else { return nil }
        
        self.id = id
        self.fullName = fullName
        self.mobileNo = mobileNo
        self.dob = dobDate
        self.imageURL = imageURL
        
        self.image = UIImage()
    }
    
    func toDictionary() -> [String: Any] {
        var result = [String: Any]()
        result["_id"] = self.id
        result["fullName"] = self.fullName
        result["mobileNo"] = self.mobileNo
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = self.dateFormatString
        result["dob"] = dateFormatter.string(from: self.dob)
        
        result["imageURL"] = self.imageURL.absoluteString
        
        return result
    }
}
