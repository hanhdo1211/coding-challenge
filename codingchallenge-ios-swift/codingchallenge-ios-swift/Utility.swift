//
//  Utility.swift
//  codingchallenge-ios-swift
//
//  Created by Hanh Do on 5/21/17.
//  Copyright © 2017 H2MobileDev. All rights reserved.
//

import Foundation

struct Utility {
    static func storePostsLocal(posts: [Post]) {
        var arrayPostData = [[String: Any]]()
        posts.forEach { (post) in
            arrayPostData.append(post.toDictionary())
        }
        print("arrayPostData \(arrayPostData)")
        let defaults = UserDefaults.standard
        defaults.set(arrayPostData, forKey: "Posts")
        print("\(defaults.object(forKey: "Posts"))")
    }
    
    static func getPostsLocal() -> [Post] {
        var result = [Post]()
        let defaults = UserDefaults.standard
        if let arrayPostData = defaults.value(forKey: "Posts") as? [[String : Any]] {
            arrayPostData.forEach({ (postData) in
                if let post = Post(json: postData) {
                    result.append(post)
                }
                
            })
        }
        return result
    }
}
