//
//  RoundedImageView.swift
//  codingchallenge-ios-swift
//
//  Created by Hanh Do on 5/17/17.
//  Copyright © 2017 H2MobileDev. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.width / 2.0
        self.clipsToBounds = true
    }
}
