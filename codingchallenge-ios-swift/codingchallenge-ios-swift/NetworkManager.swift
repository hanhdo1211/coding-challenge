//
//  RestApiManager.swift
//  codingchallenge-ios-swift
//
//  Created by Hanh Do on 5/17/17.
//  Copyright © 2017 H2MobileDev. All rights reserved.
//

import UIKit

class NetworkManager: NSObject {
    
    enum PathEnum : String {
        case GetPost = "post"
        case GetLikeCount = "post/%d/likeCount"
        case GetCommentCount = "post/%d/commentCount"
        
        func generatePath(withParameter parameter: [CVarArg]?) -> String {
            var result = ""
            if let param = parameter {
                result = String(format: self.rawValue, arguments: param)
            } else {
                result = self.rawValue
            }
            
            return result
        }
    }
    
    static let sharedInstance = NetworkManager()
    let baseURL = "http://thedemoapp.herokuapp.com/"
    
    private func makeHTTPGetRequest(path: String, onCompletion: @escaping (Any?, Error?) -> Void) {
        guard let requestURL = URL(string: path)  else {
            onCompletion(nil,nil)
            return
        }
        var request = URLRequest(url: requestURL)
//            NSMutableURLRequest(URL: NSURL(string: path)!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            if let jsonData = data,
                let json = try? JSONSerialization.jsonObject(with: jsonData, options: []),
                let jsonWithObjectRoot = json as? [String: Any]{
                guard let code = jsonWithObjectRoot["code"] as? Int, code == 200 else {
                    print("\(jsonWithObjectRoot["status"] as? String)")
                    onCompletion(nil, error)
                    return
                }
                guard let jsonData = jsonWithObjectRoot["data"] else {
                    print("can't get json data")
                    onCompletion(nil, error)
                    return
                }
                onCompletion(jsonData,error)
            } else {
                onCompletion(nil,error)
            }
        }
        
        task.resume()
    }
    
    private func getRoute(fromPath path: String) -> String {
        let route = String(format: "%@%@", arguments: [self.baseURL, path])
        return route
    }
    
    func getAllPost(onCompletion: @escaping ([Post]?) -> Void) {
        let route = getRoute(fromPath: PathEnum.GetPost.generatePath(withParameter: nil))
        makeHTTPGetRequest(path: route) { (response, error) in
            if let postArray = response as? [[String: Any]] {
                var posts = [Post]()
                postArray.forEach({ (postDict) in
                    if let post = Post(json: postDict) {
                        posts.append(post)
                    }
                })
                DispatchQueue.main.async {
                    onCompletion(posts)
                }
                
            } else {
                print("can't cast post array data")
                DispatchQueue.main.async {
                    onCompletion(nil)
                }
            }
        }
    }
    
    func getLikeCount(ofPost post: Post, onCompletion: @escaping (Int) -> Void) {
        let route = getRoute(fromPath: PathEnum.GetLikeCount.generatePath(withParameter: [post.id]))
        makeHTTPGetRequest(path: route) { (response, error) in
            if let likeCount = response as? Int {
                DispatchQueue.main.async {
                    onCompletion(likeCount)
                }
            } else {
                print("can't get like count")
                DispatchQueue.main.async {
                    onCompletion(post.likeCount)
                }
            }
        }
    }
    
    func getCommentCount(ofPost post: Post, onCompletion: @escaping (Int) -> Void) {
        let route = getRoute(fromPath: PathEnum.GetCommentCount.generatePath(withParameter: [post.id]))
        makeHTTPGetRequest(path: route) { (response, error) in
            if let commentCount = response as? Int {
                DispatchQueue.main.async {
                    onCompletion(commentCount)
                }
            } else {
                print("can't get comment count")
                DispatchQueue.main.async {
                    onCompletion(post.commentCount)
                }
            }
        }
    }
    
    // MARK: - Load image
    func imageFromURL(url: URL, onCompletion: @escaping (UIImage) -> Void) {
        // check to get local first
        if let cachedImage = AppDelegate.shared().cachedImages[url], let copyCachedImage = cachedImage.copy() as? UIImage {
            onCompletion(copyCachedImage)
            return
        }
        
        let session = URLSession.shared
        // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
        let downloadPicTask = session.dataTask(with: url) { (data, response, error) in
            // The download has finished.
            if let error = error {
                print("Error downloading picture: \(error)")
                DispatchQueue.main.async {
                    onCompletion(UIImage())
                }
                
            } else {
                if let imageData = data {
                    // Finally convert that Data into an image and do what you wish with it.
                    
                    if  let image = UIImage(data: imageData) {
                        AppDelegate.shared().cachedImages[url] = image.copy() as? UIImage
                        print("Get image success")
                        DispatchQueue.main.async {
                            onCompletion(image)
                        }
                    } else {
                        // Got data but data is wrong format, so don't need to re-download.
                        AppDelegate.shared().cachedImages[url] = UIImage()
                        print("Couldn't cast image: Data is wrong format")
                        DispatchQueue.main.async {
                            onCompletion(UIImage())
                        }
                    }
                    
                } else {
                    print("Couldn't get image: Image is nil")
                    DispatchQueue.main.async {
                        onCompletion(UIImage())
                    }
                }
            }
        }
        
        downloadPicTask.resume()
    }
}
