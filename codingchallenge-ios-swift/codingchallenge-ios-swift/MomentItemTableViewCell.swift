//
//  MomentItemTableViewCell.swift
//  codingchallenge-ios-swift
//
//  Created by Hanh Do on 5/17/17.
//  Copyright © 2017 H2MobileDev. All rights reserved.
//

import UIKit

protocol MomentItemTableViewCellDelegate: class {
    func didSelectedLikePostButton(cell: MomentItemTableViewCell)
}

class MomentItemTableViewCell: UITableViewCell {
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var postMessageLabel: UILabel!
    @IBOutlet weak var postImageImageView: UIImageView!
    @IBOutlet weak var userAvatarImageView: RoundedImageView!

    @IBOutlet weak var postLikeCountButton: UIButton!
    @IBOutlet weak var postCommentCountButton: UIButton!
    
    weak var delegate: MomentItemTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func selectedLikePostButton(_ sender: UIButton) {
        self.delegate?.didSelectedLikePostButton(cell: self)
    }
    
    func setIconPostLike(withState isLiked: Bool) {
        if isLiked == true {
            self.postLikeCountButton.setImage(#imageLiteral(resourceName: "icon_moments_like_red"), for: .normal)
        } else {
            self.postLikeCountButton.setImage(#imageLiteral(resourceName: "icon_moments_like_black"), for: .normal)
        }
    }
}
